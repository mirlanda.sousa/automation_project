const el = require('./elements').ELEMENTS;

describe  ('Search news', () =>{
    beforeEach(()=>{
        cy.wait(1000)
        cy.visit('https://arstechnica.com/')
    })

    it('Search news', () =>{
        cy.get(el.id_icon).type('Amazon begins drone deliveries in California and Texas')
        cy.get(el.id_field).type('{enter}', {force: true})

        //assert
        cy.contains(el.assert).should('be.visible')
    })
})  