# NEWS WEBSITE (Automation project)
This is a test automation project using the website https://arstechnica.com.

## Development
The language choosen to this automation project is the Cypress framework (v12).


## Build
- To clone this repository to the PC, open the terminal and input the command:
"git clone https://gitlab.com/mirlanda.sousa/automation_project.git"

* After clonning this project and opening in editor of your preference,
run the following command in the terminal: "npm install"

* To generate the test repost, run the command:
"npm i -D mochawesome"

## Tests
* Run the command "npm run cypress" and execute the automation tests. 

* To running the test report, run the command: 
"npx cypress run --reporter  mochawesome"
- After finishing the execution, report file will be generated in HTML in "report" folder.